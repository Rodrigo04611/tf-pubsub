




resource "google_pubsub_topic" "topic-a" {
  count = "${var.name-topic-a["${var.env}"] != "" ? 1 : 0}"
  name    = "${var.name-topic-a["${var.env}"]}"
}


resource "google_pubsub_subscription" "subcription-xa" {
   count = "${var.name-subscription-xa["${var.env}"] != "" ? 1 : 0}"
   name    = "${var.name-subscription-xa["${var.env}"]}"
   topic= "${var.name-topic-a["${var.env}"]}"
  }
