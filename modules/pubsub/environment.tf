##### Topic

variable "env" {
  default     = ""
  description = ""
}

variable "name-topic-a" {
  default = {
    "qa"   = "topic-qa"
    "prod" = "topic-prod"
  }
}

##### Subcription
variable "name-subscription-xa" {
  default = {
    "qa"   = "subscription-qa"
    "prod" = "subscription-prod"
  }
}
